# Tarea 1

En esta tarea debemos vincular GitLab como repositorio remoto y modificarlo.

Ya tenemos vinculado un repositorio de GitHub como repositorio remoto, por lo que debemos modificar la URL de la siguiente manera:

> git remote set-url origin https://gitlab.com/RicardoRamirezBerrocal/tarea-1.git

Una vez hecho, clonamos el repositorio de GitLab:

> git clone https://gitlab.com/RicardoRamirezBerrocal/tarea-1.git

Ya tenemos acceso al repositorio. Ahora solo hace falta añadir el archivo **index.html** y modificar el archivo **README.md** para que se documente los cambios que realizo.
Los datos se guardan de la siguiente manera:

1. Añado **index.html** usando *git add index.html*

2. Añado los cambios usando *git commit -m "COMENTARIO"*.

3. Modifico **README.md** con la información necesaria.

4. Añado los cambios igual que en el paso 2.

5. Confirmo los cambios usando *git push origin main*.

Con todo esto, hemos subido el archivo y realizado los cambios satisfactoriamente.
